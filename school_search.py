import re
import sys
import sys
import csv

def clean_text(text):
    text = text.lower()
    result = re.sub(r'\d+', '', text)
    result = result.strip()
    return result.split(" ")

def calculate_frequency(text_list,pattern_list):
    count = 0
    for each in text_list:
        if each in pattern_list:
            count+=1
    return count

def search_school(file_name, res1,e):
    with open(file_name, "r", encoding="ISO-8859-1") as quotes:
        csvReader = csv.DictReader(quotes)
        for each in csvReader:
            school = clean_text(each["SCHNAM05"])
            city = clean_text(each["LCITY05"])
            state = clean_text(each["LSTATE05"])
            res2 = school+city+state
            count = calculate_frequency(res2,res1)

            if count!=0:
                if count > e["first"][0]:
                        e["third"] = (e["second"][0] ,e["second"][1],e["second"][2],e["second"][3])
                        e["second"] = (e["first"][0],e["first"][1],e["first"][2],e["first"][3])
                        e["first"] = (count, each["SCHNAM05"],each["LCITY05"],each["LSTATE05"])
                elif count > e["second"][0]:
                        
                        e["third"] = (e["second"][0] ,e["second"][1],e["second"][2],e["second"][3])
                        e["second"] = (count,each["SCHNAM05"],each["LCITY05"],each["LSTATE05"])
                        
                elif (count > e["third"][0] ): 
                        
                        e["third"] = (count, each["SCHNAM05"],each["LCITY05"],each["LSTATE05"]) 
                        
        for k,v in e.items():
            print ("{0}, {1}, {2}".format(v[1], v[2],v[3]))
               

import time
tic = time.process_time()
text = input("Enter Search parameter: ")
res1 = clean_text(text)
third = first = second = -sys.maxsize
e = {"first":(first,None, None,None), "second":(second,None,None,None), "third":(third,None, None,None)}
search_school("school_data.csv",res1,e)
toc = time.process_time()
print (toc - tic)
