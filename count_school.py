import csv

def load_csv(file_name):
    with open(file_name, "r", encoding="ISO-8859-1") as quotes:
        csvReader = csv.DictReader(quotes)
        
        school={}
        mlocale = {}
        ulocale={}
        city={}
        l=[]
        i=0
        for e in csvReader:
            i+=1
            l.append(e["LCITY05"])
            school[e["LSTATE05"]] = school.get(e["LSTATE05"], 0) + 1
            mlocale[e["MLOCALE"]] = mlocale.get(e["MLOCALE"], 0) + 1
        
            if e["LCITY05"] not in city:
                city[e["LCITY05"]] = 1
        

        print("Total Schools: 10000: "+str(i)) 
        print ()
        print ("Schools by State:")
        for k,v in school.items():
            print("{0}: {1}".format(k,str(v)))
        print ()
        print ("Schools by Metro-centric locale:")
        for k,v in mlocale.items():
            print("{0}: {1}".format(k,str(v)))
        print ()
        a,b = most_frequent(l)
        
        print ("City with most schools: {0} ({1} schools)".format(a,b))
        print ()
        print ("Unique cities with at least one school: {0}".format(len(city)))

def most_frequent(List): 
    dict = {} 
    count, itm = 0, '' 
    for item in reversed(List): 
        dict[item] = dict.get(item, 0) + 1
        if dict[item] >= count : 
            count, itm = dict[item], item 
    return(itm,count) 
        
if __name__== "__main__":
    import time
    tic = time.process_time()
    load_csv("schhol_data.csv")
    
    toc = time.process_time()
    print (toc - tic)


